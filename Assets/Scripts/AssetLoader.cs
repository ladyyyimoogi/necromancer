﻿
using UnityEngine;

public class AssetLoader : MonoBehaviour {

    public GameObject wall;
    public GameObject tile;
    public GameObject player;
    public GameObject bullet;
    public GameObject bone;
    public GameObject lavaLamp;
    public GameObject food;
    public GameObject gate;
    public GameObject enemy;

    public GameObject enemyBullet;
    public GameObject trap;

    //for sound
    public AudioClip woof;
    public AudioClip shoot;
    public AudioClip walk;
    public AudioClip awakenV;
    public AudioClip sleepV;
    public AudioClip awaken;
    public AudioClip sleep;
    public AudioClip pizzaV;
    public AudioClip pizza;
    public AudioClip lavalampV;
    public AudioClip lavalamp;
    public AudioClip enter;
}
