﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="ScriptableObject/enemy")]
public class Enemy_SO : ScriptableObject {

    public int attackRange;
    public int detectRange;
    public int hp;
    public int attack;
    public float attackSpeed;
    public float bulletSpeed;
    public float bulletRange;
    public AudioClip sound;
    public Sprite sprite;
    public EnnumInGame.EnemyAttackType attackType;
    public Sprite bullet;
    public EnnumInGame.BulletHellPattern bulletHellType;

    public float frequency;
    public float magnitude;

}
