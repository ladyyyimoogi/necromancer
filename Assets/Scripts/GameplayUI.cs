﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayUI : MonoBehaviour {

    public Text foodTxt, totalAtkTxt, baseAtkTxt, boneAtkTxt, curBoneTxt, reqBoneTxt, lavaLampTxt,lvlTxt,msgTxt;
    
    public void ResetUI(int food,int totalAtk,int baseAtk,int reqBone,int lvl)
    {
        foodTxt.text = food.ToString();
        totalAtkTxt.text = totalAtk.ToString();
        baseAtkTxt.text = baseAtk.ToString();
        boneAtkTxt.text = "0";
        curBoneTxt.text = "0";
        reqBoneTxt.text = reqBone.ToString();
        lavaLampTxt.text = "0";
        lvlTxt.text = lvl.ToString();
        msgTxt.text = "";
    }

    public void UpdateUI(EnnumInGame.TextGUI ui,int val)
    {
        switch(ui)
        {
            case EnnumInGame.TextGUI.food: { foodTxt.text = val.ToString();break; }
            case EnnumInGame.TextGUI.totalAtk: { totalAtkTxt.text = val.ToString();break; }
            case EnnumInGame.TextGUI.boneAtk: { boneAtkTxt.text = val.ToString();break; }
            case EnnumInGame.TextGUI.curBone: { curBoneTxt.text = val.ToString();break; }
            case EnnumInGame.TextGUI.lavaLamp: { lavaLampTxt.text = val.ToString();break; }
        }
    }

    public void SetMessage(string msg)
    {
        msgTxt.text = msg;
    }
}
