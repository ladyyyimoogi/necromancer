﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

[ExecuteInEditMode]
[AddComponentMenu("Event/RightButtonEvent")]
public class RightClickEvent : MonoBehaviour{

    [System.Serializable] public class RightButton : UnityEvent { }
    public RightButton onRightDown;

    private void Awake()
    {
        onRightDown.AddListener(delegate { GetComponent<ClickableObject>().RightClickInvoked();});
    }
    
    private void LateUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            try
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
                int layerMask = LayerMask.GetMask("clickable");

                RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero, layerMask);
                if (hit != false && hit.collider != null)
                {
                    if(hit.collider.CompareTag("lavalamp")||hit.collider.CompareTag("food")||hit.collider.CompareTag("gate")||hit.collider.CompareTag("bone")|| hit.collider.CompareTag("trap"))
                    {
                        if (hit.collider.gameObject.GetComponent<ClickableObject>().id == GetComponent<ClickableObject>().id)
                        {
                            Debug.Log(hit.collider.gameObject.name);
                            onRightDown.Invoke();
                        }
                    }
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }
    }
   

}
