﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateMode : MonoBehaviour {

    public GameObject parentObj;
    /*
    private void OnMouseDown()
    {
        parentObj.GetComponent<Gate>().Enter();
    }*/
    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1))
        {
            parentObj.GetComponent<Gate>().Enter();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
    }
    private void FixedUpdate()
    {
        Vector2 pos = parentObj.transform.position;
        pos.y += 0.6f;
        transform.position = pos;
    }
}
