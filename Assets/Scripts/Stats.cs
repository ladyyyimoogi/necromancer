﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    int food;
    int totalAtk;
    int baseAtk;
    int boneAtk;
    int currBone;
    int reqBone;
    int lavalamp;
    int level;

    public void InitSetting(GameData gd)
    {
        food = gd.food;
        totalAtk = gd.totalAtk;
        baseAtk = gd.baseAtk;
        boneAtk = 0;
        currBone = 0;
        lavalamp = 0;
        level = gd.level + 1;
    }
    public void InitLevelSetting(int req)
    {
        reqBone = req;
    }
    public void ResetInitUIStats()
    {
        DungeonController.instance.uimgr.ResetUI(food, totalAtk, baseAtk, reqBone,level);
    }

    public int GetFood()
    {
        return food;
    }
    public int GetTotalAtk()
    {
        return totalAtk;
    }
    public int GetBaseAtk()
    {
        return baseAtk;
    }
    public int GetBoneAtk()
    {
        return boneAtk;
    }
    public int GetCurrBone()
    {
        return currBone;
    }
    public int GetReqBone()
    {
        return reqBone;
    }
    public int GetLavaLamp()
    {
        return lavalamp;
    }

    public void UpdateFood(int num)
    {
        food = num;
        DungeonController.instance.uimgr.UpdateUI(EnnumInGame.TextGUI.food, food);
    }
    public void UpdateBoneAtk(int num)
    {
        boneAtk = num;
        DungeonController.instance.uimgr.UpdateUI(EnnumInGame.TextGUI.boneAtk, boneAtk);
        totalAtk = baseAtk + boneAtk;
        DungeonController.instance.uimgr.UpdateUI(EnnumInGame.TextGUI.totalAtk, totalAtk);
    }
    public void UpdateCurrBone()
    {
        currBone = DungeonController.instance.boneFollowPlayerList.Count;
        DungeonController.instance.uimgr.UpdateUI(EnnumInGame.TextGUI.curBone, currBone);
    }
    public void AddLavaLamp()
    {
        lavalamp += 1;
        DungeonController.instance.uimgr.UpdateUI(EnnumInGame.TextGUI.lavaLamp, lavalamp);
    }
}
