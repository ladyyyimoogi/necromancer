﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKeyboardShortcut : MonoBehaviour {

    public delegate void PlayerDoesKeyboardInput(EnnumInGame.KeyboardShortcut key);
    public static event PlayerDoesKeyboardInput OnKeyboardInput;

	private void LateUpdate()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            OnKeyboardInputHandler(EnnumInGame.KeyboardShortcut.collectAll);
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            //enterGate
            OnKeyboardInputHandler(EnnumInGame.KeyboardShortcut.enterGate);
        }
    }

    private void OnKeyboardInputHandler(EnnumInGame.KeyboardShortcut key)
    {
        if (OnKeyboardInput != null)
            OnKeyboardInput(key);
    }
}
