﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astar {

    Vector2 src;
    Vector2 dest;
    EnnumInGame.TileType[][] map;

    List<Vector2> path=new List<Vector2>();
    List<Node> openNode = new List<Node>();
    List<Node> closedNode = new List<Node>();
    Node curr;

    List<string> nodeIdList = new List<string>();
    int offsetX;
    int offsetY;

    Stack<Node> stackNode = new Stack<Node>();

    List<Vector2> skelletonPosList = new List<Vector2>();
    bool isPathForSkelleton = false;
    List<Vector2> enemyPosList = new List<Vector2>();
    bool isPathForEnemy = false;
    
	public Astar(Vector2 sourcePos,Vector2 destinationPos)
    {
        src = sourcePos;
        dest = destinationPos;
        map = DungeonController.instance.GetMapInfo();
        offsetX = map.Length;
        offsetY = map[0].Length;
        //Debug.Log("destination:" + dest);
    }

    public List<Vector2> SearchPath()
    {
        
        Node startNode = new Node(0, CountHeuristicDistance(src,dest), null,src);
        nodeIdList.Add(startNode.id);
        openNode.Add(startNode);
        
        while(openNode.Count>0)
        {
            //sort
            openNode.Sort((x, y) => x.f.CompareTo(y.f));

            curr = openNode[0];
            openNode.Remove(curr);
            closedNode.Add(curr);
            //Debug.Log("current node:" + curr.id+" - costF:"+curr.f+" - costH:"+curr.h);

            if (curr.pos == dest)
                break;
            
            FindAdjacentNode();
            //break;
        }
        
        AddToStack(curr);
        //Debug.Log(stackNode.Count);
        AddToPath();

        return path;
    }

    public List<Vector2> SearchPathForSkelleton()
    {
        skelletonPosList.Clear();
        for (int i = 0; i < DungeonController.instance.boneFollowPlayerList.Count; i++)
        {
            Vector2 tmpPosArmy = DungeonController.instance.boneFollowPlayerList[i].transform.position;
            tmpPosArmy.x = Mathf.RoundToInt(tmpPosArmy.x);
            tmpPosArmy.y = Mathf.RoundToInt(tmpPosArmy.y);
            if (tmpPosArmy != src)
                skelletonPosList.Add(tmpPosArmy);
        }
        isPathForSkelleton = true;

        return SearchPath();
        
    }

    public List<Vector2> SearchPathForEnemy()
    {
        enemyPosList.Clear();
        for (int i = 0; i < DungeonController.instance.enemyList.Count; i++)
        {
            Vector2 tmpPosArmy = DungeonController.instance.enemyList[i].transform.position;
            tmpPosArmy.x = Mathf.RoundToInt(tmpPosArmy.x);
            tmpPosArmy.y = Mathf.RoundToInt(tmpPosArmy.y);
            if (tmpPosArmy != src)
            {
                enemyPosList.Add(tmpPosArmy);
                Vector2 floatPos = DungeonController.instance.enemyList[i].transform.position;
                if (floatPos.y > tmpPosArmy.y)
                {
                    enemyPosList.Add(new Vector2(tmpPosArmy.x, tmpPosArmy.y + 1));
                    if (floatPos.x > tmpPosArmy.x)
                        enemyPosList.Add(new Vector2(tmpPosArmy.x + 1, tmpPosArmy.y + 1));
                    else if (floatPos.x < tmpPosArmy.x)
                        enemyPosList.Add(new Vector2(tmpPosArmy.x - 1, tmpPosArmy.y + 1));
                }
                else if (floatPos.y < tmpPosArmy.y)
                {
                    enemyPosList.Add(new Vector2(tmpPosArmy.x, tmpPosArmy.y - 1));
                    if (floatPos.x > tmpPosArmy.x)
                        enemyPosList.Add(new Vector2(tmpPosArmy.x + 1, tmpPosArmy.y - 1));
                    else if (floatPos.x < tmpPosArmy.x)
                        enemyPosList.Add(new Vector2(tmpPosArmy.x - 1, tmpPosArmy.y - 1));
                }
            }
            
        }
        isPathForEnemy = true;
        return SearchPath();
    }

    private float CountHeuristicDistance(Vector2 source, Vector2 destination)
    {
        float hDist = 0;
        float xSource = source.x;
        float ySource = source.y;
        float xDestination = destination.x;
        float yDestination = destination.y;

        hDist = Mathf.Sqrt(Mathf.Pow(Mathf.Abs(xSource - xDestination),2) + Mathf.Pow(Mathf.Abs(ySource - yDestination),2));
        return hDist;
    }

    private void FindAdjacentNode()
    {
        int tmpX = curr.posX;
        int tmpY = curr.posY;

        if (tmpY + 1 < offsetY)
        {
            string tmpId = tmpX.ToString() + "x" + (tmpY + 1).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //northeast
        if (tmpY + 1 < offsetY && tmpX + 1 < offsetX)
        {
            string tmpId = (tmpX+1).ToString() + "x" + (tmpY + 1).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //east 
        if (tmpX + 1 < offsetX)
        {
            string tmpId = (tmpX + 1).ToString() + "x" + (tmpY).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //southeast
        if (tmpX + 1 < offsetX && tmpY - 1 >= 0)
        {
            string tmpId = (tmpX + 1).ToString() + "x" + (tmpY - 1).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //south
        if (tmpY - 1 >= 0)
        {
            string tmpId = (tmpX).ToString() + "x" + (tmpY - 1).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //southwest
        if (tmpX - 1 >= 0 && tmpY - 1 >= 0)
        {
            string tmpId = (tmpX - 1).ToString() + "x" + (tmpY - 1).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //west
        if (tmpX - 1 >= 0)
        {
            string tmpId = (tmpX-1).ToString() + "x" + (tmpY).ToString();
            ObserveAdjacentNode(tmpId);
        }
        //northwest
        if (tmpX - 1 >= 0 && tmpY + 1 < offsetY)
        {
            string tmpId = (tmpX - 1).ToString() + "x" + (tmpY + 1).ToString();
            ObserveAdjacentNode(tmpId);
        }

    }

    private void ObserveAdjacentNode(string key)
    {
        string[] posStr = key.Split('x');
        if (map[int.Parse(posStr[0])][int.Parse(posStr[1])] == EnnumInGame.TileType.floor)
        {
            //Debug.Log("observe " + key);
            if (IsNodeInClosed(key) == false)
            {

                if (IsNodeInOpen(key) == true)
                {
                    int idx = FindIndexNodeInOpen(key);
                    float newGdist = curr.g + (Mathf.Sqrt(Mathf.Pow(Mathf.Abs(curr.posX - int.Parse(posStr[0])), 2) + Mathf.Pow(Mathf.Abs(curr.posY - int.Parse(posStr[1])), 2)));
                    
                    if (openNode[idx].g > newGdist)
                    {
                        openNode[idx].UpdateNewVal(newGdist, curr);
                    }

                    if (isPathForSkelleton)
                    {
                        if (skelletonPosList.Contains(openNode[idx].pos))
                            openNode[idx].f += 5;
                    }
                    if (isPathForEnemy)
                    {
                        if (enemyPosList.Contains(openNode[idx].pos))
                            openNode[idx].f += 5;
                    }
                }
                else
                {
                    float gDist = curr.g + (Mathf.Sqrt(Mathf.Pow(Mathf.Abs(curr.posX - int.Parse(posStr[0])), 2) + Mathf.Pow(Mathf.Abs(curr.posY - int.Parse(posStr[1])), 2)));
                    Vector2 tmpPos = new Vector2(float.Parse(posStr[0]), float.Parse(posStr[1]));
                    float hDist = CountHeuristicDistance(tmpPos, dest);
                    Node newNode = new Node(gDist, hDist, curr, tmpPos);
                    if (isPathForSkelleton)
                    {
                        if (skelletonPosList.Contains(tmpPos))
                            newNode.f += 5;
                    }
                    if (isPathForEnemy)
                    {
                        if (enemyPosList.Contains(tmpPos))
                            newNode.f += 5;
                    }
                    openNode.Add(newNode);
                    nodeIdList.Add(newNode.id);
                }
            }
        }
        
    }

    private bool IsNodeInClosed(string key)
    {
        if(nodeIdList.Contains(key))
        {
            for (int i = 0; i < closedNode.Count; i++)
            {
                Node tmpClosed = closedNode[i];
                if (tmpClosed.id == key)
                    return true;
            }
            
        }
        return false;
    }

    private bool IsNodeInOpen(string key)
    {
        if (nodeIdList.Contains(key))
        {
            for (int i = 0; i < openNode.Count; i++)
            {
                Node tmpOpen= openNode[i];
                if (tmpOpen.id == key)
                    return true;
            }

        }
        return false;
    }

    private int FindIndexNodeInOpen(string key)
    {
        int idx = 0;
        for (int i = 0; i < openNode.Count; i++)
        {
            if (openNode[i].id == key)
                idx = i;
        }
        return idx;
    }

    private void AddToStack(Node node)
    {
        stackNode.Push(node);
        if (node.parent != null)
            AddToStack(node.parent);
    }

    private void AddToPath()
    {
        Node pathNode = stackNode.Pop();
        //Debug.Log(pathNode.posX.ToString() + " " + pathNode.posY.ToString());
        path.Add(pathNode.pos);
        if (stackNode.Count>0)
            AddToPath();
    }
}
