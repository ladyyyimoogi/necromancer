﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public Vector3 currentPosition;
    public Vector3 destPosition;
    bool bulletIsRunning = false;
    float speed = 4f;
    int damage = 0;

    public void RunBullet()
    {
        bulletIsRunning = true;
        damage = DungeonController.instance.player.GetComponent<Player>().GetDamage();
        DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.shoot);
    }
    public void LateUpdate()
    {
        if(bulletIsRunning==true)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), destPosition, speed * Time.deltaTime);

            if (transform.position == destPosition)
            {
                bulletIsRunning = false;
                DungeonController.instance.player.GetComponent<Player>().ReturnBulletToPool(this.gameObject);
            }
        }
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("player")||collision.collider.CompareTag("bone")|| collision.collider.CompareTag("mode")|| collision.collider.CompareTag("lavalamp")|| collision.collider.CompareTag("food"))
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<CircleCollider2D>());
        }
        if(collision.collider.CompareTag("wall"))
        {
            //Debug.Log("collide");
            bulletIsRunning = false;
            DungeonController.instance.player.GetComponent<Player>().ReturnBulletToPool(this.gameObject);
        }
        if(collision.collider.CompareTag("enemy"))
        {
            collision.collider.gameObject.GetComponent<Enemy>().GetShot(damage);
            bulletIsRunning = false;
            DungeonController.instance.player.GetComponent<Player>().ReturnBulletToPool(this.gameObject);
        }
        if(collision.collider.CompareTag("enemyBullet"))
        {
            bulletIsRunning = false;
            DungeonController.instance.player.GetComponent<Player>().ReturnBulletToPool(this.gameObject);
        }
    }
}
