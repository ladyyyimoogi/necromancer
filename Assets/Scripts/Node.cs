﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node{

    public float f;
    public float g;
    public float h;

    public Node parent = null;

    public int posX;
    public int posY;
    public string id;
    public Vector2 pos;

	public Node(float gVal,float hVal,Node parentNode,Vector2 pos)
    {
        g = gVal;
        h = hVal;
        f = g + h;
        parent = parentNode;
        posX = Mathf.FloorToInt(pos.x);
        posY = Mathf.FloorToInt(pos.y);
        id = posX.ToString() + "x" + posY.ToString();
        this.pos = pos;
    }

    public void UpdateNewVal(float gVal,Node newParentNode)
    {
        g = gVal;
        f = g + h;
        parent = newParentNode;
    }
}
