﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour {

    float speed;
    Vector2 init;
    Vector2 dest;
    int damage;
    float range;
    bool bulletIsRunning = false;
    EnnumInGame.BulletType type;

    float frequency=15.0f;
    float magnitude=0.5f;
    float multiplier;
    Vector2 formerPos;
    Vector2 axis;
    EnnumInGame.DirectionBullet directionBullet;

	public void RunBullet(float speed,int damage,float range,Vector2 destinationPos)
    {
        init = transform.position;
        this.speed = speed;
        this.dest = destinationPos;
        this.damage = damage;
        this.range = range;
        bulletIsRunning = true;
        type = EnnumInGame.BulletType.directional;
        
    }

    public void RunBulletPattern(float speed, int damage, float range,float freq,float mag,float multiplier,EnnumInGame.DirectionBullet dir)
    {
        //
        init = transform.position;
        this.speed = speed;
        
        this.damage = damage;
        this.range = range;
        this.frequency = freq;
        this.magnitude = mag;
        bulletIsRunning = true;
        type = EnnumInGame.BulletType.wave;
        formerPos = init;
        
        if (dir == EnnumInGame.DirectionBullet.horizontal)
            axis = new Vector2(0, 1f);
        else if (dir == EnnumInGame.DirectionBullet.vertical)
            axis = new Vector2(1, 0);
        this.multiplier = multiplier;
        this.directionBullet = dir;
    }

    public void LateUpdate()
    {
        if (bulletIsRunning == true)
        {
            if(type==EnnumInGame.BulletType.directional)
            {
                transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), dest, speed * Time.deltaTime);

            }
            else
            {
                if(directionBullet==EnnumInGame.DirectionBullet.vertical)
                    formerPos += (Vector2)transform.up * Time.deltaTime * speed * multiplier;
                else if(directionBullet==EnnumInGame.DirectionBullet.horizontal)
                    formerPos += (Vector2)transform.right * Time.deltaTime * speed * multiplier;
                transform.position = formerPos + axis * Mathf.Sin(Time.time * frequency) * magnitude;
            }

            if (Mathf.Abs(Vector2.Distance(transform.position,init))>=range)
            {
                bulletIsRunning = false;
                DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
            }

            if ((Vector2)transform.position == dest && type==EnnumInGame.BulletType.directional)
            {
                bulletIsRunning = false;
                DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!collision.collider.CompareTag("player")&&!collision.collider.CompareTag("bone")/*&&!collision.collider.CompareTag("wall")*/)
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }
        /*
        if(collision.collider.CompareTag("wall"))
        {
            bulletIsRunning = false;
            DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
        }*/
        if(collision.collider.CompareTag("player"))
        {
            collision.collider.GetComponent<Player>().GetAttacked(damage);
            bulletIsRunning = false;
            DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
        }
        if(collision.collider.CompareTag("bullet"))
        {
            bulletIsRunning = false;
            DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
        }
        if(collision.collider.CompareTag("bone"))
        {
            //collision buat change to sleep di bone........
            if(collision.collider.GetComponent<Bone>().mode==EnnumInGame.BoneMode.awaken)
            {
                bulletIsRunning = false;
                DungeonController.instance.enemyBulletPool.ReturnBulletToPool(this.gameObject);
            }
            else
            {
                Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
            }
        }
    }
}
