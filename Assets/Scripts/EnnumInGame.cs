﻿

public class EnnumInGame {

    public enum TileType { none, floor, wall }
    public enum Direction { north, east, south, west, northEast, southEast, southWest, northWest }
    public enum BoneMode { sleep,awaken}
    public enum TextGUI { food,totalAtk,baseAtk,boneAtk,curBone,reqBone,lavaLamp}
    public enum Mode { normal,hard}
    public enum Sound { woof,shoot,walk,awaken,sleep,pizza,lavalamp,enter,hungry}
    public enum EnemyAttackType { melee,shooter,bullethell,poison,spawner,hook,devolving }

    public enum KeyboardShortcut { collectAll,enterGate}
    public enum BulletHellPattern { none,cross,x,oneWave,crossWave,xWave,circle,verticalWave,horizontalWave}
    public enum BulletType { directional, wave}
    public enum DirectionBullet { none,horizontal,vertical}
}
