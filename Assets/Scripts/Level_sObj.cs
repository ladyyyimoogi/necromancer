﻿
using UnityEngine;

[CreateAssetMenu(menuName ="ScriptableObject/Level")]
public class Level_sObj : ScriptableObject {
    public int rows;
    public int columns;
    public int numRoomMin;
    public int numRoomMax;
    public int roomWidthMin;
    public int roomWidthMax;
    public int roomHeightMin;
    public int roomHeightMax;
    public int corridorLengthMin;
    public int corridorLengthMax;

    public int minimumSummon;
    public int spreadLava;
    public int spreadBone;
    public int spreadFood;
    public int spreadEnemy;
    public int spreadTrap;

    public Enemy_SO[] enemySO;
	
}
