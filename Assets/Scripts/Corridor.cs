﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corridor
{

    public int startXpos;
    public int startYpos;
    public int corridorLength;
    public EnnumInGame.Direction direction;

    public void SetUpCorridor(Room room, IntRange length, IntRange roomWidth, IntRange roomHeight, int columns, int rows, bool firstCorridor)
    {
        direction = (EnnumInGame.Direction)Random.Range(0, 4);
        EnnumInGame.Direction oppositeDirection = (EnnumInGame.Direction)(((int)room.enteringCorridor + 2) % 4);

        if (!firstCorridor && direction == oppositeDirection)
        {
            int tmpDirection = (int)direction;
            tmpDirection++;
            tmpDirection = tmpDirection % 4;
            direction = (EnnumInGame.Direction)tmpDirection;
        }

        corridorLength = length.Random();
        int maxLength = length.m_Max;

        switch (direction)
        {
            case EnnumInGame.Direction.north:
                startXpos = Random.Range(room.xPos, room.xPos + room.roomWidth - 1);
                startYpos = room.yPos + room.roomHeight;
                maxLength = rows - startYpos - roomHeight.m_Min;
                break;
            case EnnumInGame.Direction.east:
                startXpos = room.xPos + room.roomWidth;
                startYpos = Random.Range(room.yPos, room.yPos + room.roomHeight - 1);
                maxLength = columns - startXpos - roomWidth.m_Min;
                break;
            case EnnumInGame.Direction.south:
                startXpos = Random.Range(room.xPos, room.xPos + room.roomWidth);
                startYpos = room.yPos;
                maxLength = startYpos - roomHeight.m_Min;
                break;
            case EnnumInGame.Direction.west:
                startXpos = room.xPos;
                startYpos = Random.Range(room.yPos, room.yPos + room.roomHeight);
                maxLength = startXpos - roomWidth.m_Min;
                break;
        }

        corridorLength = Mathf.Clamp(corridorLength, 1, maxLength);
    }

    public int EndPositionX()
    {
        if (direction == EnnumInGame.Direction.north || direction == EnnumInGame.Direction.south)
        {
            return startXpos;
        }
        else if (direction == EnnumInGame.Direction.east)
        {
            return startXpos + corridorLength - 1;
        }
        else
        {
            return startXpos - corridorLength + 1;
        }
    }

    public int EndPositionY()
    {
        if (direction == EnnumInGame.Direction.east || direction == EnnumInGame.Direction.west)
        {
            return startYpos;
        }
        else if (direction == EnnumInGame.Direction.north)
        {
            return startYpos + corridorLength - 1;
        }
        else
        {
            return startYpos - corridorLength + 1;
        }
    }
}
