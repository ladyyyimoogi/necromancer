﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : ClickableObject {

    //bool canBeDisarmed = false;

    public override void ShowBubble()
    {
        //canBeDisarmed = true;
        //Debug.Log("walk into trap");
    }
    public override void HideBubble()
    {
        //Debug.Log("trap is not in range");
    }
    public override void RightClickInvoked()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("player"))
            DungeonController.instance.player.AddComponent<Player>().GetAttacked(5);
        if(collision.gameObject.CompareTag("bone"))
        {
            if(collision.gameObject.GetComponent<Bone>().mode==EnnumInGame.BoneMode.awaken)
            {
                collision.gameObject.GetComponent<Bone>().mode = EnnumInGame.BoneMode.sleep;
                collision.gameObject.GetComponent<SpriteRenderer>().sprite = collision.gameObject.GetComponent<Bone>().boneSleep;
                DungeonController.instance.boneFollowPlayerList.Remove(collision.gameObject);
                DungeonController.instance.player.GetComponent<Player>().SubtractAttackFromBone();
                DungeonController.instance.stats.UpdateCurrBone();
            }
        }
        
    }
}
