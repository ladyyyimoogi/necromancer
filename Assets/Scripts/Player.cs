﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    List<GameObject> bulletPool = new List<GameObject>();
    private GameObject bulletHolder;

    int baseFood = 100;
    int baseAttack = 1;
    int addAttack = 0;
    
    private void Awake()
    {
        baseFood = DungeonController.instance.stats.GetFood();
        baseAttack = DungeonController.instance.stats.GetBaseAtk();
        addAttack = DungeonController.instance.stats.GetBoneAtk();
    }

    public void CreateBulletPool(GameObject bullet)
    {
        bulletHolder = new GameObject("BulletHolder");
        for (int i = 0; i < 100; i++)
        {
            Vector3 pos= new Vector3(-1000, -1000, -10);
            GameObject tmpBullet = Instantiate(bullet, pos, Quaternion.identity);
            tmpBullet.transform.parent = bulletHolder.transform;
            tmpBullet.SetActive(false);
            bulletPool.Add(tmpBullet);
        }
    }

    public GameObject GetBulletFromPool()
    {
        GameObject tmpBullet = bulletPool[0];
        bulletPool.RemoveAt(0);
        tmpBullet.SetActive(true);
        return tmpBullet;
    }

    public void ReturnBulletToPool(GameObject bullet)
    {
        Vector3 pos = new Vector3(-1000, -1000, -10);
        bullet.transform.position = pos;
        bullet.SetActive(false);
        bulletPool.Add(bullet);
    }

    public void AddAttackFromBone()
    {
        addAttack += 1;
        DungeonController.instance.stats.UpdateBoneAtk(addAttack);
    }
    public void SubtractAttackFromBone()
    {
        addAttack -= 1;
        DungeonController.instance.stats.UpdateBoneAtk(addAttack);
    }
    public int GetDamage()
    {
        return baseAttack + addAttack;
    }

    public void ShootBullet()
    {
        baseFood -= 1;
        DungeonController.instance.stats.UpdateFood(baseFood);
        CheckLife();
    }

    public void GetAttacked(int damage)
    {
        baseFood -= damage;
        DungeonController.instance.stats.UpdateFood(baseFood);
        CheckLife();
    }

    public int GetBaseFood()
    {
        return baseFood;
    }

    public void AddFood(int food)
    {
        baseFood += food;
        DungeonController.instance.stats.UpdateFood(baseFood);
    }
    public void ConsumeFood(int food)
    {
        baseFood -= food;
        DungeonController.instance.stats.UpdateFood(baseFood);
    }

    private void CheckLife()
    {
        if (baseFood <= 0)
        {
            Debug.Log("game over");
            DungeonController.instance.GameOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("bullet"))
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        //if(collision.collider.CompareTag("mode"))
            //Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        if (collision.collider.CompareTag("bone"))
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        if (collision.collider.CompareTag("lavalamp"))
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        if (collision.collider.CompareTag("food"))
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        if (collision.CompareTag("bone"))
            collision.gameObject.GetComponent<Bone>().ShowBubble();
        if (collision.CompareTag("lavalamp"))
        {
            //Debug.Log("lavalamp trigger");
            collision.gameObject.GetComponent<LavaLamp>().ShowBubble();
        }
        if (collision.CompareTag("food"))
            collision.gameObject.GetComponent<Food>().ShowBubble();
        if (collision.CompareTag("gate"))
            collision.gameObject.GetComponent<Gate>().ShowBubble();*/
        if(collision.CompareTag("enemy"))
        {
            collision.gameObject.GetComponent<Enemy>().SetAware();
        }
        //if (collision.CompareTag("mode"))
            //collision.gameObject.transform.parent.GetComponent<ClickableObject>().ShowBubble();
            //collision.gameObject.GetComponent<BoneMode>().ShowBubble();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (collision.CompareTag("mode"))
          //  collision.gameObject.transform.parent.GetComponent<ClickableObject>().HideBubble();
        /*
        if (collision.CompareTag("bone"))
            collision.gameObject.GetComponent<Bone>().HideBubble();
        if (collision.CompareTag("lavalamp"))
            collision.gameObject.GetComponent<LavaLamp>().HideBubble();
        if (collision.CompareTag("food"))
            collision.gameObject.GetComponent<Food>().HideBubble();
        if (collision.CompareTag("gate"))
            collision.gameObject.GetComponent<Gate>().HideBubble();
            */

    }
}
