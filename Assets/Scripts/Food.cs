﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food: ClickableObject
{

   // string id;
    public GameObject childObj;
    public Sprite childSprite;
    int add = 20;
    private void Awake()
    {
        //childObj = this.gameObject.transform.GetChild(0).gameObject;
    }

    private void OnEnable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput += HandleKeyboardInput;
    }

    private void OnDisable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput -= HandleKeyboardInput;
    }

    private void HandleKeyboardInput(EnnumInGame.KeyboardShortcut key)
    {
        if (key == EnnumInGame.KeyboardShortcut.collectAll && DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            Take();
    }
    
    public override void ShowBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = childSprite;
        if (!DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Add(this.gameObject);
    }
    public override void HideBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = null;
        if (DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Remove(this.gameObject);
    }
    
    public override void RightClickInvoked()
    {
        
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            Take();
    }
    public void Take()
    {
        //add to inventory and blablabla
        DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.pizza);
        DungeonController.instance.player.GetComponent<Player>().AddFood(add);
        if (DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Remove(this.gameObject);
        //this.gameObject.SetActive(false);
        Destroy(this.gameObject);
    }

    
}
