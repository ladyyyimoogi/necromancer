﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : ClickableObject{

    
    public GameObject childObj;
    public Sprite childSprite;

    bool canEnter = false;

    private void Awake()
    {
        //childObj = this.gameObject.transform.GetChild(0).gameObject;
    }

    private void OnEnable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput += HandleKeyboardInput;
    }

    private void OnDisable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput -= HandleKeyboardInput;
    }

    private void HandleKeyboardInput(EnnumInGame.KeyboardShortcut key)
    {
        if (key == EnnumInGame.KeyboardShortcut.enterGate && canEnter == true)
            Enter();
    }

    public override void ShowBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = childSprite;
        canEnter = true;
    }
    public override void HideBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = null;
        canEnter = false;
    }
    /*
    private void OnMouseDown()
    {
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            Enter();
    }*/
    
    public override void RightClickInvoked()
    {
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            Enter();
    }
    public void Enter()
    {
        //to nextlevel
        //Debug.Log("enter");
        if(DungeonController.instance.stats.GetCurrBone() >= DungeonController.instance.stats.GetReqBone())
        {
            if(DungeonController.instance.stats.GetCurrBone()>DungeonController.instance.stats.GetLavaLamp())
            {
                string msg = "Each skeletons need to have their own lavalamp to pass the gate. Find more lavalamps or put your skeleton to sleep";
                DungeonController.instance.uimgr.SetMessage(msg);
            }
            else
            {
                DungeonController.instance.NextGame();
            }
        }
        else
        {
            int val = DungeonController.instance.stats.GetReqBone();
            string msg = "You need at least " + val + " skeletons to enter the gate";
            DungeonController.instance.uimgr.SetMessage(msg);
        }
    }
    
}
