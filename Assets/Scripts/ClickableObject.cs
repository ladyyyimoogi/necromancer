﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClickableObject : MonoBehaviour {

    public abstract void RightClickInvoked();
    public abstract void ShowBubble();
    public abstract void HideBubble();

    public string id="gate";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("player"))
            ShowBubble();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("player"))
            HideBubble();
    }

    public void SetId()
    {
        id = transform.position.x + "-" + transform.position.y;
    }
    
}
