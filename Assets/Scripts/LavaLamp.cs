﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaLamp : ClickableObject {

    
    public GameObject childObj;
    public Sprite childSprite;


    /*
    private void OnMouseDown()
    {
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            Take();
    }
    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1))
        {
            if (childObj.GetComponent<SpriteRenderer>().sprite != null)
                Take();
        }
    }*/

    private void OnEnable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput += HandleKeyboardInput;
    }

    private void OnDisable()
    {
        PlayerKeyboardShortcut.OnKeyboardInput -= HandleKeyboardInput;
    }

    private void HandleKeyboardInput(EnnumInGame.KeyboardShortcut key)
    {
        if (key == EnnumInGame.KeyboardShortcut.collectAll && DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            Take();
    }

    public override void ShowBubble()
    {
        //Debug.Log("lavalamp show bubble");
        childObj.GetComponent<SpriteRenderer>().sprite = childSprite;
        if (!DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Add(this.gameObject);
    }
    public override void HideBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = null;
        if (DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Remove(this.gameObject);
    }
    public void Take()
    {
        //add to inventory and blablabla
        DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.lavalamp);
        DungeonController.instance.stats.AddLavaLamp();
        if (DungeonController.instance.itemToAutocollect.Contains(this.gameObject))
            DungeonController.instance.itemToAutocollect.Remove(this.gameObject);
        //this.gameObject.SetActive(false);
        Destroy(this.gameObject);
    }
    public override void RightClickInvoked()
    {

        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            Take();
    }

}
