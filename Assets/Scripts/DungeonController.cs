﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DungeonController : MonoBehaviour {

    public static DungeonController instance { get; set; }

    public AssetLoader assetLoader;
    LevelManager levelMgr;
    BoardCreator board;
    SaveLoad fileMgr;
    GameData gameData;
    HungerClock hungermgr;
    public Stats stats;
    public GameplayUI uimgr;
    public SoundManager soundMgr;

    public EnemyBulletPool enemyBulletPool;

    //untuk sementara
    int currentLevel = 0;
    public GameObject player;
    public List<GameObject> boneFollowPlayerList = new List<GameObject>();
    public List<GameObject> enemyList = new List<GameObject>();
    public List<GameObject> itemToAutocollect = new List<GameObject>();
    
    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;

        assetLoader = GetComponent<AssetLoader>();
        levelMgr = GetComponent<LevelManager>();
        board = GetComponent<BoardCreator>();
        fileMgr = GetComponent<SaveLoad>();
        stats = GetComponent<Stats>();
        uimgr = GetComponent<GameplayUI>();
        hungermgr = GetComponent<HungerClock>();
        soundMgr = GetComponent<SoundManager>();
        enemyBulletPool = GetComponent<EnemyBulletPool>();

        LoadSettings();
        StartThisLevel();
    }

    private void LateUpdate()
    {
        if(Time.frameCount%30==0)
            System.GC.Collect();
    }

    private void LoadSettings()
    {
        if (fileMgr.CheckIfExists("necro.dat"))
            gameData = fileMgr.LoadFile<GameData>("necro.dat") as GameData;
        else
            gameData = new GameData();

        fileMgr.SaveFile<GameData>(gameData, "necro.dat");

        currentLevel = gameData.level;
        stats.InitSetting(gameData);
        stats.InitLevelSetting(levelMgr.levelSetting[currentLevel].minimumSummon);
        stats.ResetInitUIStats();

        if (gameData.mode == EnnumInGame.Mode.hard)
            hungermgr.StartClock();
    }

    private void StartThisLevel()
    {
        PrepareEnemyActPool();
        board.CreateDungeon(levelMgr.levelSetting[currentLevel], assetLoader.tile, assetLoader.wall);
        InitPlayer();
        board.PopulateDungeon();
        soundMgr.PlaySound(EnnumInGame.Sound.enter);
        
    }

    private void InitPlayer()
    {
        Room tmpRoom = board.GetFirstRoom();
        Vector2 posPlayer = new Vector2(tmpRoom.xPos, tmpRoom.yPos);
        player = Instantiate(assetLoader.player, posPlayer, Quaternion.identity);
        player.GetComponent<Player>().CreateBulletPool(assetLoader.bullet as GameObject);
        
        Camera.main.transform.position = new Vector3(posPlayer.x, posPlayer.y, Camera.main.transform.position.z);
    }

    private void PrepareEnemyActPool()
    {
        enemyBulletPool.CreateBulletPool(assetLoader.enemyBullet);
    }

    public void NextGame()
    {
        gameData.level += 1;
        gameData.food = stats.GetFood();
        gameData.totalAtk = stats.GetTotalAtk();
        gameData.baseAtk =gameData.totalAtk;
        fileMgr.SaveFile<GameData>(gameData, "necro.dat");
        
        if(gameData.level<levelMgr.levelSetting.Length)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene(3);
    }

    //=======================================
    public EnnumInGame.TileType[][] GetMapInfo()
    {
        return board.GetMapInfo();
    }

    public List<GameObject> GetBonePosition()
    {
        return boneFollowPlayerList;
    }

    public int[] GetOffsetXY()
    {
        int[] offset = { board.GetOffsetX(),board.GetOffsetY() };
        return offset;
    }

    public bool CheckIfFloor(int x,int y)
    {
        return board.CheckIfFloor(x, y);
    }

    public float GetAcceptableDistance()
    {
        return 0.5f + (boneFollowPlayerList.Count * 0.55f);
    }
}
