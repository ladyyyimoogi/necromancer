﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public Button normalBtn, nightmareBtn;
    SaveLoad fileMgr;

    private void Awake()
    {
        fileMgr = GetComponent<SaveLoad>();
        if (fileMgr.CheckIfExists("necro.dat"))
            fileMgr.DeleteFile<GameData>("necro.dat");

        normalBtn.onClick.AddListener(delegate { StartGame(EnnumInGame.Mode.normal); });
        nightmareBtn.onClick.AddListener(delegate { StartGame(EnnumInGame.Mode.hard); });
    }

    private void StartGame(EnnumInGame.Mode mode)
    {
        GameData gamedata = new GameData();
        gamedata.mode = mode;
        fileMgr.SaveFile<GameData>(gamedata, "necro.dat");
        SceneManager.LoadScene(4);
    }
}
