﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungerClock : MonoBehaviour {

    //hungry every 20s warning start from 10s
    int num = 10;
    bool canEat = false;

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(canEat==true && DungeonController.instance.boneFollowPlayerList.Count > 0)
            {
                CancelInvoke("KillAll");
                CancelInvoke("KillMessage");
                StartClock();
                DungeonController.instance.player.GetComponent<Player>().ConsumeFood(DungeonController.instance.boneFollowPlayerList.Count);
                DungeonController.instance.uimgr.SetMessage("");
                canEat = false;
            }
        }
    }

    public void StartClock()
    {
        num = 10;
        Invoke("KillAll", 20);
        Invoke("KillMessage", 10);
    }

    private void KillAll()
    {
        if(DungeonController.instance.boneFollowPlayerList.Count>0)
        {
            for (int i = DungeonController.instance.boneFollowPlayerList.Count - 1; i >= 0; i--)
            {
                DungeonController.instance.boneFollowPlayerList[i].GetComponent<Bone>().TooHungry();
            }
        }
        
    }

    private void KillMessage()
    {
        canEat = true;
        if (DungeonController.instance.boneFollowPlayerList.Count > 0)
        {
            string msg = "skeletons: WE'RE HUNGRY! (Press 'space' to feed them. timeleft:" + num + "s)";
            DungeonController.instance.uimgr.SetMessage(msg);
        }
        num--;
        if (num > 0)
            Invoke("KillMessage", 1);

    }
}
