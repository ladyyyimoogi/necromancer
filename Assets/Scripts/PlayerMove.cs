﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    float speed = 4f;

    bool walk = false;
    bool walkOnKey = false;
    //bool canCallDetectNow = true;

    Vector2 destinationPos;
    List<Vector2> pathPos=new List<Vector2>();

    public delegate void PlayerHasNewDirection();
    public static event PlayerHasNewDirection OnNewDirection;

    private void OnEnable()
    {
        BtnTileListener.OnTileClicked += HandleTileClick;
    }

    private void OnDisable()
    {
        BtnTileListener.OnTileClicked -= HandleTileClick;
    }
    

    private void HandleTileClick(int addX,int addY)
    {
        Vector2 sourcePos = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        Vector2 gotoPos = new Vector2(sourcePos.x+addX, sourcePos.y+addY);
        //add offlimit... and check
        int[] offset = DungeonController.instance.GetOffsetXY();
        if(gotoPos.x<offset[0]&&gotoPos.y<offset[1])
        {
            if(gotoPos.x>=0 && gotoPos.y>=0)
            {
                if(DungeonController.instance.CheckIfFloor(Mathf.RoundToInt(gotoPos.x),Mathf.RoundToInt(gotoPos.y))==true)
                {
                    //Debug.Log("JALAN");
                    PlayerWalkTo(Mathf.RoundToInt(gotoPos.x), Mathf.RoundToInt(gotoPos.y));
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (walk&&pathPos.Count>0)
        {
            transform.position = Vector2.MoveTowards(transform.position, pathPos[0], speed * Time.deltaTime);
            Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);

            if ((Vector2)transform.position == pathPos[0])
            {
                pathPos.RemoveAt(0);
                Invoke("OnNewDirectionHandler", 2f);
            }
            if (pathPos.Count == 0)
                walk = false;
        }

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        if(h!=0 ||v!=0)
        {
            walk = false;
            pathPos.Clear();
            Vector2 newPos = new Vector2(transform.position.x + h, transform.position.y + v);
            transform.position = Vector2.MoveTowards(transform.position, newPos, Time.deltaTime * speed);
            Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        }
        if(Input.GetKeyDown(KeyCode.UpArrow)|| Input.GetKeyDown(KeyCode.DownArrow)|| Input.GetKeyDown(KeyCode.LeftArrow)|| Input.GetKeyDown(KeyCode.RightArrow))
        {
            
            
            if (walkOnKey == false)
            {
                walkOnKey = true;
                CheckNewDirectionByWalkingOnKey();
            }
            
        }
        else if(Input.GetKeyUp(KeyCode.UpArrow) && Input.GetKeyUp(KeyCode.DownArrow) && Input.GetKeyUp(KeyCode.LeftArrow) && Input.GetKeyUp(KeyCode.RightArrow))
        {
            walkOnKey = false;
            CancelInvoke("CheckNewDirectionByWalkingOnKey");
            
        }
        //InvokeRepeating("CheckNewDirectionByWalkingOnKey", 0, 3);

    }

    public void PlayerWalkTo(int coordX,int coordY)
    {
        
        destinationPos= new Vector2(coordX, coordY);
        walk = true;
        Vector2 sourcePos = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        if(sourcePos!=destinationPos)
        {
            Astar astar = new Astar(sourcePos, destinationPos);
            pathPos = astar.SearchPath();
            

        }
        Invoke("OnNewDirectionHandler", 0.5f);

    }

    private void CheckNewDirectionByWalkingOnKey()
    {
        if (walkOnKey == true)
        {
            OnNewDirectionHandler();
            
        }
        Invoke("CheckNewDirectionByWalkingOnKey", 2);
    }
    

    public void OnNewDirectionHandler()
    {
        if (OnNewDirection != null)
            OnNewDirection();
    }
    
}
