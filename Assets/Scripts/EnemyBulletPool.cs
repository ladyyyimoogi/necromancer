﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletPool : MonoBehaviour {

    List<GameObject> bulletPool=new List<GameObject>();
    GameObject enemyBulletHolder;
    
    public void CreateBulletPool(GameObject bullet)
    {
        enemyBulletHolder = new GameObject("enemyBulletHolder");
        for (int i = 0; i < 100; i++)
        {
            Vector3 pos = new Vector3(-1000, -1000, -10);
            GameObject tmpBullet = Instantiate(bullet, pos, Quaternion.identity);
            tmpBullet.transform.parent = enemyBulletHolder.transform;
            tmpBullet.SetActive(false);
            bulletPool.Add(tmpBullet);
        }
        //Debug.Log("done pooling enemy bullet");
    }
    public GameObject GetBulletFromPool()
    {
        GameObject tmpBullet = bulletPool[0];
        bulletPool.RemoveAt(0);
        tmpBullet.SetActive(true);
        return tmpBullet;
    }

    public void ReturnBulletToPool(GameObject bullet)
    {
        Vector3 pos = new Vector3(-1000, -1000, -10);
        bullet.transform.position = pos;
        bullet.SetActive(false);
        bulletPool.Add(bullet);
    }
}
