﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BoneMode : MonoBehaviour
{

    public EnnumInGame.BoneMode mode;
    public bool visible = false;
    public GameObject parentObj;

    /*
    private void OnMouseDown()
    {
        parentObj.GetComponent<Bone>().ChangeMode();
    }*/
    /*
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
            parentObj.GetComponent<Bone>().ChangeMode();
    }*/

    private void FixedUpdate()
    {
        Vector2 pos = parentObj.transform.position;
        pos.y += 0.6f;
        transform.position = pos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
    }

}
