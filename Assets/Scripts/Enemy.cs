﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    
    string id;
    Transform playerTarget;
    bool aware = false;
    bool shootRange = false;
    float playerDistance = 1f;
    float speed = 3f;
    public bool isCollide = false;

    public int hp = 10;
    int attack = 1;
    bool turnLeft = true;
    int rangeDistance = 1;
    float attackSpeed = 0;
    float bulletSpeed = 0;
    float bulletRange = 0;
    float detectRange = 0;
    EnnumInGame.EnemyAttackType attackType;
    EnnumInGame.BulletHellPattern bulletPattern=EnnumInGame.BulletHellPattern.none;
    float freq;
    float mag;

    Vector2 moveBackPos = new Vector2();
    Vector2 destinationPos;
    List<Vector2> pathPos = new List<Vector2>();
    Enemy_SO so;

    float lastBulletTime;
    Vector2 tmpPosBeforeShoot;
    
    private void Awake()
    {
        playerTarget = DungeonController.instance.player.transform;
        moveBackPos = transform.position;
        lastBulletTime = 0;
    }

    public void SetEnemySO(Enemy_SO so)
    {
        this.so = so;
        this.hp = so.hp;
        this.attack = so.attack;
        this.rangeDistance = so.attackRange;
        this.attackSpeed = so.attackSpeed;
        this.attackType = so.attackType;
        this.detectRange = so.detectRange;
        GetComponent<SpriteRenderer>().sprite = so.sprite;

        //buat testing sebelom pake sprite beda
        if (attackType == EnnumInGame.EnemyAttackType.shooter)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            //this.attackSpeed = so.attackSpeed;
            this.bulletSpeed = so.bulletSpeed;
            this.bulletRange = so.bulletRange;
            this.playerDistance = so.attackRange;
        }
        else if(attackType==EnnumInGame.EnemyAttackType.bullethell)
        {
            GetComponent<SpriteRenderer>().color = Color.black;
            //this.attackSpeed = so.attackSpeed;
            this.bulletSpeed = so.bulletSpeed;
            this.bulletRange = so.bulletRange;
            this.playerDistance = so.attackRange;
            this.bulletPattern = so.bulletHellType;
            this.freq = so.frequency;
            this.mag = so.magnitude;
        }

        GetComponent<CircleCollider2D>().radius = this.detectRange;
    }
   
    private void OnEnable()
    {
        PlayerMove.OnNewDirection += HandleNewDirection;
    }
    private void OnDisable()
    {
        PlayerMove.OnNewDirection -= HandleNewDirection;
    }

    private void HandleNewDirection()
    {
        if(aware)
        {
            //SearchNecromancer();
        }
    }

    private void Start()
    {
        float distTmp = Mathf.Abs(Vector2.Distance(transform.position,playerTarget.position));
        if (distTmp <= detectRange)
            SetAware();

        if((attackType==EnnumInGame.EnemyAttackType.shooter||attackType==EnnumInGame.EnemyAttackType.bullethell) &&aware)
        {
            InvokeRepeating("ShootBulletToPlayer", 0, attackSpeed);
        }
    }

    public void FixedUpdate()
    {
        if (aware)
        {
            if (playerTarget.transform.position.x < transform.position.x)
            {
                if(turnLeft==false)
                {
                    Vector3 tmpScale = transform.localScale;
                    tmpScale.x *= -1;
                    transform.localScale = tmpScale;
                    turnLeft = true;
                }
            }
            else
            {
                if(turnLeft==true)
                {
                    Vector3 tmpScale = transform.localScale;
                    tmpScale.x *= -1;
                    transform.localScale = tmpScale;
                    turnLeft = false;
                }
            }

            if (Mathf.Abs(Vector2.Distance(transform.position,playerTarget.position))>1.2f && Mathf.Abs(Vector2.Distance(transform.position, moveBackPos))>1)
                moveBackPos = (Vector2)transform.position;
            
            if (pathPos.Count > 0 && attackType==EnnumInGame.EnemyAttackType.melee /*&& Mathf.Abs(Vector2.Distance(transform.position, playerTarget.position)) > playerDistance*/)
            {
                transform.position = Vector2.MoveTowards(transform.position, pathPos[0], speed * Time.deltaTime);

                if ((Vector2)transform.position == pathPos[0])
                {
                    //formerPos = pathPos[0];
                    pathPos.RemoveAt(0);
                    SearchNecromancer();
                }

            }
            else if(pathPos.Count>0 && (attackType==EnnumInGame.EnemyAttackType.shooter||attackType==EnnumInGame.EnemyAttackType.bullethell))
            {
                if(Mathf.Abs(Vector2.Distance(transform.position,playerTarget.position))>playerDistance)
                {
                    transform.position = Vector2.MoveTowards(transform.position, pathPos[0], speed * Time.deltaTime);

                    if ((Vector2)transform.position == pathPos[0])
                    {
                        //formerPos = pathPos[0];
                        pathPos.RemoveAt(0);
                        SearchNecromancer();
                    }
                }
            }
            

            if((attackType==EnnumInGame.EnemyAttackType.shooter||attackType==EnnumInGame.EnemyAttackType.bullethell) && Mathf.Abs(Vector2.Distance(transform.position,playerTarget.position))<=rangeDistance)
            {
                if(shootRange==false)
                {
                    //CancelInvoke("ShootBulletToPlayer");
                    //ShootBulletToPlayer();
                    shootRange = true;
                    InvokeRepeating("ShootBulletToPlayer", 0, attackSpeed);
                }
            }
            else if((attackType==EnnumInGame.EnemyAttackType.shooter||attackType==EnnumInGame.EnemyAttackType.bullethell) && Mathf.Abs(Vector2.Distance(transform.position, playerTarget.position)) > rangeDistance)
            {
                shootRange = false;
                CancelInvoke("ShootBulletToPlayer");
                SearchNecromancer();
            }

            if (pathPos.Count == 0)
                SearchNecromancer();
        }
    }

    public void GetShot(int damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            //this.gameObject.SetActive(false);
            CancelInvoke("ShootBulletToPlayer");
            CancelInvoke("AttackMelee");
            DungeonController.instance.enemyList.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!collision.collider.CompareTag("player")&& !collision.collider.CompareTag("bone")&& !collision.collider.CompareTag("enemy"))
        {
            Physics2D.IgnoreCollision(collision.collider, this.gameObject.GetComponent<Collider2D>());
        }
        if(collision.collider.CompareTag("player"))
        {
            //collision.collider.gameObject.GetComponent<Player>().GetAttacked(attack);
            //DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.woof);
            //MoveBack();
            if (attackType == EnnumInGame.EnemyAttackType.melee)
                InvokeRepeating("AttackMelee", 0, attackSpeed);
            isCollide = true;
        }
        if(collision.collider.CompareTag("bone"))
        {
            if (collision.collider.gameObject.GetComponent<Bone>().mode == EnnumInGame.BoneMode.sleep)
                Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
            else
            {
                MoveBack();
                DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.woof);
            }
        }
        if(collision.collider.CompareTag("enemy"))
        {
            
            if (attackType == EnnumInGame.EnemyAttackType.melee)
            {
                if (Mathf.Abs(Vector2.Distance(playerTarget.position, this.gameObject.transform.position)) >= Mathf.Abs(Vector2.Distance(playerTarget.position, collision.collider.gameObject.transform.position)))
                {
                    //this.transform.position = pathPos[0];
                    //MoveBackCollide();
                    MoveBack();
                    SearchNecromancer();
                }
            }
            if (attackType==EnnumInGame.EnemyAttackType.shooter)
            {
                if (Mathf.Abs(Vector2.Distance(playerTarget.position, this.gameObject.transform.position)) < Mathf.Abs(Vector2.Distance(playerTarget.position, collision.collider.gameObject.transform.position)))
                {
                    //this.transform.position = pathPos[0];
                    if(pathPos.Count>1)
                        MoveForwardCollide();
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("player"))
        {
            if (attackType == EnnumInGame.EnemyAttackType.melee)
                CancelInvoke("AttackMelee");
            isCollide = false;
        }
    }

    private void MoveBack()
    {
        transform.position = moveBackPos;
    }
    
    private void MoveForwardCollide()
    {
        Vector2.MoveTowards(transform.position, pathPos[0], speed * Time.deltaTime);
        pathPos.RemoveAt(0);
    }

    public void SetAware()
    {
        aware = true;
        SearchNecromancer();
        DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.woof);
    }

    private void SearchNecromancer()
    {
        destinationPos = new Vector2(Mathf.RoundToInt(playerTarget.position.x), Mathf.RoundToInt(playerTarget.position.y));

        Vector2 sourcePos = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        if (sourcePos != destinationPos)
        {
            Astar astar = new Astar(sourcePos, destinationPos);
            pathPos = astar.SearchPathForEnemy();
            pathPos.RemoveAt(0);
            //walk = true;

        }
    }

    private void ShootBulletToPlayer()
    {
        //get bullet from pool and shoot
        tmpPosBeforeShoot = transform.position;
        pathPos.Clear();
        

        if (Time.timeSinceLevelLoad-lastBulletTime>=0.1f)
        {
            lastBulletTime = Time.timeSinceLevelLoad;
            if (attackType == EnnumInGame.EnemyAttackType.shooter)
                ShooterShoot();
            else if (attackType == EnnumInGame.EnemyAttackType.bullethell)
                BulletHellShoot();

            Invoke("ResetPos", 0.1f);
        }
        
        //call again if still in range
        
    }

    private void ShooterShoot()
    {
        
        GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
        tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
        tmpBullet.transform.position = transform.position;
        tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, DungeonController.instance.player.transform.position);
        
    }

    private void BulletHellShoot()
    {
        switch(bulletPattern)
        {
            case EnnumInGame.BulletHellPattern.cross:
                {
                    //Debug.Log("test pattern cross");
                    Vector2 tmpDestination = new Vector2(transform.position.x, transform.position.y + bulletRange);
                    GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x, transform.position.y - bulletRange);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x+bulletRange, transform.position.y);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x - bulletRange, transform.position.y);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    break;
                }
            case EnnumInGame.BulletHellPattern.x:
                {
                    Vector2 tmpDestination = new Vector2(transform.position.x+bulletRange, transform.position.y + bulletRange);
                    GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x+bulletRange, transform.position.y - bulletRange);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x - bulletRange, transform.position.y+bulletRange);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);

                    tmpDestination = new Vector2(transform.position.x - bulletRange, transform.position.y-bulletRange);
                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBullet(bulletSpeed, attack, bulletRange, tmpDestination);
                    break;
                }
            case EnnumInGame.BulletHellPattern.circle:
                {
                    ///----help?
                    break;
                }
            case EnnumInGame.BulletHellPattern.verticalWave:
                {
                    GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange,freq,mag,1,EnnumInGame.DirectionBullet.vertical);

                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, -1,EnnumInGame.DirectionBullet.vertical);
                    break;
                }
            case EnnumInGame.BulletHellPattern.horizontalWave:
                {
                    GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, 1, EnnumInGame.DirectionBullet.horizontal);

                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, -1, EnnumInGame.DirectionBullet.horizontal);
                    break;
                }
            case EnnumInGame.BulletHellPattern.crossWave:
                {
                    GameObject tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, 1, EnnumInGame.DirectionBullet.vertical);

                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, -1, EnnumInGame.DirectionBullet.vertical);

                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, 1, EnnumInGame.DirectionBullet.horizontal);

                    tmpBullet = DungeonController.instance.enemyBulletPool.GetBulletFromPool();
                    tmpBullet.GetComponent<SpriteRenderer>().sprite = so.bullet;
                    tmpBullet.transform.position = transform.position;
                    tmpBullet.GetComponent<BulletEnemy>().RunBulletPattern(bulletSpeed, attack, bulletRange, freq, mag, -1, EnnumInGame.DirectionBullet.horizontal);
                    break;
                }
        }
    }

    private void AttackMelee()
    {
        DungeonController.instance.player.GetComponent<Player>().GetAttacked(attack);
    }

    private void ResetPos()
    {
        transform.position = tmpPosBeforeShoot;
    }
}
