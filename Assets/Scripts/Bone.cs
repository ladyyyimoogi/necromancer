﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bone : ClickableObject {

    public Sprite boneSleep;
    public Sprite boneAwake;
    public Sprite modeSleep;
    public Sprite modeAwake;

    public GameObject childObj;
    Transform target;

    public EnnumInGame.BoneMode mode = EnnumInGame.BoneMode.sleep;

    //public override string id;
    float speed = 3f;
    float distance = 1f;

    Vector2 destinationPos;
    bool walk = false;
    List<Vector2> pathPos = new List<Vector2>();
    Vector2 formerPos;


    private void OnEnable()
    {
        PlayerMove.OnNewDirection += HandleNewDirection;
    }

    private void OnDisable()
    {
        PlayerMove.OnNewDirection -= HandleNewDirection;
    }

    private void HandleNewDirection()
    {
        if (mode == EnnumInGame.BoneMode.awaken)
            SearchNecromancer();
    }
    

    public override void ShowBubble()
    {
        childObj.GetComponent<BoneMode>().mode = this.mode;
        childObj.GetComponent<BoneMode>().visible = true;
        if (mode==EnnumInGame.BoneMode.sleep)
        {
            childObj.GetComponent<SpriteRenderer>().sprite = modeAwake;
        }
        else
        {
            childObj.GetComponent<SpriteRenderer>().sprite = modeSleep;
        }
    }
    /*
    private void OnMouseDown()
    {
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            ChangeMode();
    }
    */
    /*
    private void OnMouseOver()
    {
        
        if(Input.GetMouseButton(1))
        {
            Debug.Log("clicked");
            if (childObj.GetComponent<SpriteRenderer>().sprite != null)
                ChangeMode();
        }
    }*/

    public override void RightClickInvoked()
    {
        if (childObj.GetComponent<SpriteRenderer>().sprite != null)
            ChangeMode();
    }

    public void ChangeMode()
    {
        if(mode==EnnumInGame.BoneMode.sleep)
        {
            mode = EnnumInGame.BoneMode.awaken;
            formerPos = transform.position;
            GetComponent<SpriteRenderer>().sprite = boneAwake;
            target = DungeonController.instance.player.transform;
            DungeonController.instance.boneFollowPlayerList.Add(this.gameObject);
            DungeonController.instance.player.GetComponent<Player>().AddAttackFromBone();
            DungeonController.instance.player.GetComponent<Player>().ConsumeFood(3);
            DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.awaken);
            SearchNecromancer();
        }
        else
        {
            mode = EnnumInGame.BoneMode.sleep;
            GetComponent<SpriteRenderer>().sprite = boneSleep;
            DungeonController.instance.boneFollowPlayerList.Remove(this.gameObject);
            DungeonController.instance.player.GetComponent<Player>().SubtractAttackFromBone();
            DungeonController.instance.player.GetComponent<Player>().ConsumeFood(1);
            DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.sleep);
            CancelInvoke("SearchNecromancer");
            walk = false;
        }
        DungeonController.instance.stats.UpdateCurrBone();
    }

    public void TooHungry()
    {
        mode = EnnumInGame.BoneMode.sleep;
        GetComponent<SpriteRenderer>().sprite = boneSleep;
        DungeonController.instance.boneFollowPlayerList.Remove(this.gameObject);
        DungeonController.instance.player.GetComponent<Player>().SubtractAttackFromBone();
        DungeonController.instance.soundMgr.PlaySound(EnnumInGame.Sound.hungry);
        walk = false;
        CancelInvoke("SearchNecromancer");
        DungeonController.instance.stats.UpdateCurrBone();
    }

    public override void HideBubble()
    {
        childObj.GetComponent<SpriteRenderer>().sprite = null;
        childObj.GetComponent<BoneMode>().visible = true;
    }

    public void FixedUpdate()
    {
        if (walk&& mode == EnnumInGame.BoneMode.awaken&&pathPos.Count>0&&Mathf.Abs(Vector2.Distance(transform.position,target.position))>distance)
        {
            transform.position = Vector2.MoveTowards(transform.position, pathPos[0], speed * Time.deltaTime);

            if ((Vector2)transform.position == pathPos[0])
            {
                formerPos = pathPos[0];
                pathPos.RemoveAt(0);
                SearchNecromancer();
            }
            
        }
        else if(walk && mode == EnnumInGame.BoneMode.awaken && Mathf.Abs(Vector2.Distance(target.position,transform.position))<=distance)
        {
            walk = false;
        }

        
        /*
        if(mode==EnnumInGame.BoneMode.awaken)
        {
            if(Vector2.Distance(transform.position,target.position)>distance)
                transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target.position, speed * Time.deltaTime);
            //Debug.Log("try to follow:" + target.position);
        }*/
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.collider.CompareTag("mode"))
        //  Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());

        if (mode == EnnumInGame.BoneMode.sleep)
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        
        if(collision.collider.CompareTag("enemy")||collision.collider.CompareTag("enemyBullet")|| collision.collider.CompareTag("trap"))
        {
            if(mode==EnnumInGame.BoneMode.sleep)
            {
                Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
            }
            else
            {
                mode = EnnumInGame.BoneMode.sleep;
                GetComponent<SpriteRenderer>().sprite = boneSleep;
                DungeonController.instance.boneFollowPlayerList.Remove(this.gameObject);
                DungeonController.instance.player.GetComponent<Player>().SubtractAttackFromBone();
                DungeonController.instance.stats.UpdateCurrBone();
            }
        }
        if(collision.collider.CompareTag("player")&&mode==EnnumInGame.BoneMode.awaken)
        {
            float time = (UnityEngine.Random.Range(1, 10)) / 10;
            Invoke("SearchNecromancer", time);
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }
        if(collision.collider.CompareTag("bone")&&mode==EnnumInGame.BoneMode.awaken && collision.collider.gameObject.GetComponent<Bone>().mode==EnnumInGame.BoneMode.awaken)
        {

            if(Mathf.Abs(Vector2.Distance(target.position,this.gameObject.transform.position))>DungeonController.instance.GetAcceptableDistance())
            {
                if(Mathf.Abs(Vector2.Distance(target.position,this.gameObject.transform.position))>=Mathf.Abs(Vector2.Distance(target.position,collision.collider.gameObject.transform.position)) && Mathf.Abs(Vector2.Distance(target.position, this.gameObject.transform.position)) > distance)
                {
                    //this.transform.position = pathPos[0];
                    MoveBack();
                }
                //float time = (Random.Range(1, 10)) / 10;
                //Invoke("MoveBack", time);
            }
            
            //SearchNecromancer();
            
        }
        if(collision.collider.CompareTag("bone")&& collision.collider.gameObject.GetComponent<Bone>().mode==EnnumInGame.BoneMode.sleep)
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        } 

        if(collision.collider.CompareTag("food")|| collision.collider.CompareTag("gate")|| collision.collider.CompareTag("lavalamp"))
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("bone") && mode == EnnumInGame.BoneMode.awaken && collision.collider.gameObject.GetComponent<Bone>().mode == EnnumInGame.BoneMode.awaken)
        {

            if (Mathf.Abs(Vector2.Distance(target.position, this.gameObject.transform.position)) > DungeonController.instance.GetAcceptableDistance())
            {
                if (Mathf.Abs(Vector2.Distance(target.position, this.gameObject.transform.position)) >= Mathf.Abs(Vector2.Distance(target.position, collision.collider.gameObject.transform.position)) && Mathf.Abs(Vector2.Distance(target.position, this.gameObject.transform.position)) > distance)
                {
                    //this.transform.position = pathPos[0];
                    MoveBack();
                }
                //float time = (Random.Range(1, 10)) / 10;
                //Invoke("MoveBack", time);
            }

            //SearchNecromancer();

        }
    }
    private void MoveBack()
    {
        //Debug.Log("moveback");
        pathPos[0] = formerPos;
    }

    private void SearchNecromancer()
    {
        destinationPos = new Vector2(Mathf.RoundToInt(target.position.x),Mathf.RoundToInt(target.position.y));
        
        Vector2 sourcePos= new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        if (sourcePos != destinationPos)
        {
            Astar astar = new Astar(sourcePos, destinationPos);
            pathPos = astar.SearchPathForSkelleton();
            pathPos.RemoveAt(0);
            walk = true;

        }
        //Invoke("SearchNecromancer", 1f);

    }
    
}
