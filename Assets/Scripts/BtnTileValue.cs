﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnTileValue : MonoBehaviour {

    public int xAdd;
    public int yAdd;

    Button thisBtn;

    private void Awake()
    {
        xAdd = Mathf.FloorToInt(GetComponent<RectTransform>().localPosition.x / 100);
        yAdd = Mathf.FloorToInt(GetComponent<RectTransform>().localPosition.y / 100);

        thisBtn = GetComponent<Button>();
        thisBtn.onClick.AddListener(delegate { DungeonController.instance.GetComponent<BtnTileListener>().DistanceFromPlayer(xAdd, yAdd); });
    }
}
