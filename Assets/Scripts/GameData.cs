﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData : System.Object {

    public int level = 0;
    public int food = 100;
    public int totalAtk = 1;
    public int baseAtk = 1;
    public int boneAtk = 0;
    public EnnumInGame.Mode mode = EnnumInGame.Mode.normal;

}
