﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnTileListener : MonoBehaviour {

    public delegate void PlayerClickOnTile(int x, int y);
    public static event PlayerClickOnTile OnTileClicked;

    int xCoordAdd;
    int yCoordAdd;

    public void DistanceFromPlayer(int x, int y)
    {
        //Debug.Log("add x:" + x + " - add y:" + y);
        xCoordAdd = x;
        yCoordAdd = y;
        

        if (OnTileClicked != null)
        {
            OnTileClicked(xCoordAdd, yCoordAdd);
        }
    }
}
