﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class SaveLoad : MonoBehaviour {
    BinaryFormatter bf;
    FileStream file;

    public bool CheckIfExists(string path)
    {
        if (File.Exists(Application.persistentDataPath + "/" + path))
            return true;
        else
            return false;
    }

    public System.Object LoadFile<T>(string path)
    {
        bf = new BinaryFormatter();
        file = File.Open(Application.persistentDataPath + "/" + path, FileMode.Open);
        T type = (T)bf.Deserialize(file);
        file.Close();
        return type;
    }

    public void SaveFile<T>(object myObj, string path)
    {
        bf = new BinaryFormatter();
        file = File.Create(Application.persistentDataPath + "/" + path);
        T objT = (T)myObj;
        bf.Serialize(file, objT);
        file.Close();
    }

    public void DeleteFile<T>(string path)
    {
        bool _isExist = CheckIfExists(path);
        if (_isExist)
        {
            File.Delete(Application.persistentDataPath + "/" + path);
        }
    }
}
