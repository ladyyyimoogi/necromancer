﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCreator : MonoBehaviour {

    public int columns;
    public int rows;

    public IntRange numRoom;
    public IntRange roomWidth;
    public IntRange roomHeight;
    public IntRange corridorLength;

    public GameObject floorTile;
    public GameObject wallTile;

    public EnnumInGame.TileType[][] tiles;

    public Room[] rooms;
    public Corridor[] corridors;
    private GameObject boardHolder;
    private GameObject objHolder;

    int spreadBone;
    int spreadEnemy;
    int spreadLavaLamp;
    int spreadFood;
    int spreadTrap;

    List<string> posTileList = new List<string>();
    Level_sObj level;

    public void CreateDungeon(Level_sObj levelSet,GameObject floor,GameObject wall)
    {
        boardHolder = new GameObject("BoardHolder");
        objHolder = new GameObject("ObjHolder");
        this.floorTile = floor;
        this.wallTile = wall;
        this.level = levelSet;

        SetUpSettings();
        SetUpTilesArray();
        CreateRoomsAndCorridors();
        SetTilesValuesForRooms();
        SetTilesValuesForCorridors();
        InstantiateFloorTiles();
        InstantiateWallTiles();
        InstantiateWallInBorder();
        //PopulateDungeon();
    }

    private void SetUpSettings()
    {
        this.columns = level.columns;
        this.rows = level.rows;
        this.numRoom = new IntRange(level.numRoomMin, level.numRoomMax);
        this.roomWidth = new IntRange(level.roomWidthMin, level.roomWidthMax);
        this.roomHeight = new IntRange(level.roomHeightMin, level.roomHeightMax);
        this.corridorLength = new IntRange(level.corridorLengthMin, level.corridorLengthMax);

        this.spreadBone = level.spreadBone;
        this.spreadEnemy = level.spreadEnemy;
        this.spreadLavaLamp = level.spreadLava;
        this.spreadFood = level.spreadFood;
        this.spreadTrap = level.spreadTrap;
    }

    private void SetUpTilesArray()
    {
        tiles = new EnnumInGame.TileType[columns][];
        
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i] = new EnnumInGame.TileType[rows];
            
        }

    }

    private void CreateRoomsAndCorridors()
    {
        rooms = new Room[numRoom.Random()];
        corridors = new Corridor[rooms.Length - 1];

        rooms[0] = new Room();
        corridors[0] = new Corridor();

        rooms[0].SetUpRoom(roomWidth, roomHeight, columns, rows);
        corridors[0].SetUpCorridor(rooms[0], corridorLength, roomWidth, roomHeight, columns, rows, true);

        for (int i = 1; i < rooms.Length; i++)
        {
            rooms[i] = new Room();
            rooms[i].SetUpRoom(roomWidth, roomHeight, columns, rows, corridors[i - 1]);

            if (i < corridors.Length)
            {
                corridors[i] = new Corridor();
                corridors[i].SetUpCorridor(rooms[i], corridorLength, roomWidth, roomHeight, columns, rows, false);
            }
        }
    }

    private void SetTilesValuesForRooms()
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            Room currentRoom = rooms[i];
            for (int j = 0; j < currentRoom.roomWidth; j++)
            {
                int xCoord = currentRoom.xPos + j;
                for (int k = 0; k < currentRoom.roomHeight; k++)
                {
                    int yCoord = currentRoom.yPos + k;
                    tiles[xCoord][yCoord] = EnnumInGame.TileType.floor;
                }
            }
        }
    }

    private void SetTilesValuesForCorridors()
    {
        for (int i = 0; i < corridors.Length; i++)
        {
            Corridor currentCorridor = corridors[i];

            for (int j = 0; j < currentCorridor.corridorLength; j++)
            {
                int xCoord = currentCorridor.startXpos;
                int yCoord = currentCorridor.startYpos;

                switch (currentCorridor.direction)
                {
                    case EnnumInGame.Direction.north:
                        {
                            yCoord += j;
                            break;
                        }
                    case EnnumInGame.Direction.east:
                        {
                            xCoord += j;
                            break;
                        }
                    case EnnumInGame.Direction.south:
                        {
                            yCoord -= j;
                            break;
                        }
                    case EnnumInGame.Direction.west:
                        {
                            xCoord -= j;
                            break;
                        }
                }
                tiles[xCoord][yCoord] = EnnumInGame.TileType.floor;
            }
        }
    }

    private void InstantiateFloorTiles()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[i].Length; j++)
            {

                if (tiles[i][j] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(floorTile, i, j);
                    string tmpPos = i + "|" + j;
                    posTileList.Add(tmpPos);
                }
            }
        }
    }

    private void InstantiateWallTiles()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            for (int j = 0; j < tiles[i].Length; j++)
            {
                if (tiles[i][j] != EnnumInGame.TileType.floor)
                {
                    if (CheckIfWall(i, j))
                    {
                        InstantiateTile(wallTile, i, j);
                        tiles[i][j] = EnnumInGame.TileType.wall;
                    }
                }
            }
        }
    }

    private bool CheckIfWall(int xCoord, int yCoord)
    {
        if (xCoord + 1 < columns && tiles[xCoord + 1][yCoord] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (xCoord - 1 >= 0 && tiles[xCoord - 1][yCoord] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (yCoord + 1 < rows && tiles[xCoord][yCoord + 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (yCoord - 1 >= 0 && tiles[xCoord][yCoord - 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (xCoord + 1 < columns && yCoord + 1 < rows && tiles[xCoord + 1][yCoord + 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (xCoord + 1 < columns && yCoord - 1 >= 0 && tiles[xCoord + 1][yCoord - 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (xCoord - 1 >= 0 && yCoord + 1 < rows && tiles[xCoord - 1][yCoord + 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        if (xCoord - 1 >= 0 && yCoord - 1 >= 0 && tiles[xCoord - 1][yCoord - 1] == EnnumInGame.TileType.floor)
        {
            return true;
        }
        return false;
    }

    private void InstantiateWallInBorder()
    {
        //top - bottom
        for (int i = -1; i <= columns; i++)
        {
            if (i != -1 && i != columns)
            {
                if (tiles[i][columns - 1] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, i, columns);
                }
                if (tiles[i][0] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, i, -1);
                }
            }
            if (i == -1)
            {
                if (tiles[0][columns - 1] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, -1, columns);
                }
                if (tiles[0][0] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, -1, -1);
                }
            }
            if (i == columns)
            {
                if (tiles[rows - 1][columns - 1] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, rows, columns);
                }
                if (tiles[rows - 1][0] == EnnumInGame.TileType.floor)
                {
                    InstantiateTile(wallTile, rows, -1);
                }
            }
        }

        //left-right
        for (int i = 0; i < rows; i++)
        {
            if (tiles[0][i] == EnnumInGame.TileType.floor)
            {
                InstantiateTile(wallTile, -1, i);
            }
            if (tiles[columns - 1][i] == EnnumInGame.TileType.floor)
            {
                InstantiateTile(wallTile, columns, i);
            }
        }
    }

    private void InstantiateTile(GameObject prefabs, float xCoord, float yCoord)
    {
        //int randomIdx = Random.Range(0, prefabs.Length);
        Vector3 pos = new Vector3(xCoord, yCoord, 1);
        GameObject tileInstance = Instantiate(prefabs, pos, Quaternion.identity) as GameObject;
        tileInstance.transform.parent = boardHolder.transform;
    }

    public void PopulateDungeon()
    {
        string playerPos = rooms[0].xPos + "|" + rooms[0].yPos;
        Vector2 tmpPlayerPos = new Vector2(rooms[0].xPos, rooms[0].yPos);
        float distance = Mathf.Sqrt(Mathf.Pow(rooms[0].roomHeight, 2) + Mathf.Pow(rooms[0].roomWidth, 2));
        posTileList.Remove(playerPos);

        //gate
        int idxRoom = Random.Range(1, rooms.Length);
        string gatePos = rooms[idxRoom].xPos + "|" + rooms[idxRoom].yPos;
        Vector3 gatev3 = new Vector3(rooms[idxRoom].xPos, rooms[idxRoom].yPos, 1);
        GameObject gate = Instantiate(DungeonController.instance.assetLoader.gate, gatev3, Quaternion.identity) as GameObject;
        gate.transform.parent = objHolder.transform;
        posTileList.Remove(gatePos);

        while(spreadBone>0)
        {
            int idx = Random.Range(0, posTileList.Count);
            string[] tmpPos = posTileList[idx].Split('|');
            Vector3 pos = new Vector3(int.Parse(tmpPos[0]), int.Parse(tmpPos[1]), 1);
            GameObject obj = Instantiate(DungeonController.instance.assetLoader.bone, pos, Quaternion.identity) as GameObject;
            obj.transform.parent = objHolder.transform;
            obj.GetComponent<ClickableObject>().SetId();
            posTileList.RemoveAt(idx);
            spreadBone--;
        }

        while(spreadLavaLamp>0)
        {
            int idx = Random.Range(0, posTileList.Count);
            string[] tmpPos = posTileList[idx].Split('|');
            Vector3 pos = new Vector3(int.Parse(tmpPos[0]), int.Parse(tmpPos[1]), 1);
            GameObject obj = Instantiate(DungeonController.instance.assetLoader.lavaLamp, pos, Quaternion.identity) as GameObject;
            obj.transform.parent = objHolder.transform;
            obj.GetComponent<ClickableObject>().SetId();
            posTileList.RemoveAt(idx);
            spreadLavaLamp--;
        }
        while (spreadFood > 0)
        {
            int idx = Random.Range(0, posTileList.Count);
            string[] tmpPos = posTileList[idx].Split('|');
            Vector3 pos = new Vector3(int.Parse(tmpPos[0]), int.Parse(tmpPos[1]), 1);
            GameObject obj = Instantiate(DungeonController.instance.assetLoader.food, pos, Quaternion.identity) as GameObject;
            obj.transform.parent = objHolder.transform;
            obj.GetComponent<ClickableObject>().SetId();
            posTileList.RemoveAt(idx);
            spreadFood--;
        }

        while(spreadEnemy>0)
        {
            float tmpDistance = 0;
            int enemyX = 0;
            int enemyY = 0;
            int idx=0;
            while(tmpDistance<distance)
            {
                idx = Random.Range(0, posTileList.Count);
                string[] tmpPos = posTileList[idx].Split('|');
                enemyX = int.Parse(tmpPos[0]);
                enemyY = int.Parse(tmpPos[1]);
                tmpDistance = Mathf.Sqrt(Mathf.Pow(Mathf.Abs(enemyX - tmpPlayerPos.x), 2) + Mathf.Pow(Mathf.Abs(enemyY - tmpPlayerPos.y), 2));
            }
            
            Vector3 pos = new Vector3(enemyX, enemyY, 1);
            GameObject obj = Instantiate(DungeonController.instance.assetLoader.enemy, pos, Quaternion.identity) as GameObject;
            obj.GetComponent<Enemy>().SetEnemySO(level.enemySO[Random.Range(0, level.enemySO.Length)]);
            obj.transform.parent = objHolder.transform;
            DungeonController.instance.enemyList.Add(obj);
            posTileList.RemoveAt(idx);
            spreadEnemy--;
        }

        while(spreadTrap>0)
        {

            int idx = Random.Range(0, posTileList.Count);
            string[] tmpPos = posTileList[idx].Split('|');
            Vector3 pos = new Vector3(int.Parse(tmpPos[0]), int.Parse(tmpPos[1]), 1);
            GameObject obj = Instantiate(DungeonController.instance.assetLoader.trap, pos, Quaternion.identity) as GameObject;
            obj.transform.parent = objHolder.transform;
            obj.GetComponent<ClickableObject>().SetId();
            posTileList.RemoveAt(idx);
            spreadTrap--;
        }
        
        
    }

    //----------------------------------------

    public Room GetFirstRoom()
    {
        return rooms[0];
    }

    public EnnumInGame.TileType[][] GetMapInfo()
    {
        return tiles;
    }

    public int GetOffsetX()
    {
        return tiles.Length;
    }

    public int GetOffsetY()
    {
        return tiles[0].Length;
    }

    public bool CheckIfFloor(int x, int y)
    {
        if (tiles[x][y] == EnnumInGame.TileType.floor)
            return true;
        else
            return false;
    }
}
