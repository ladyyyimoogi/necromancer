﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    AudioSource audio;
    
    public void PlaySound(EnnumInGame.Sound sound)
    {
        audio = GetComponent<AudioSource>();
        
        switch(sound)
        {
            case EnnumInGame.Sound.woof: { audio.PlayOneShot(DungeonController.instance.assetLoader.woof,0.3f); break; }
            case EnnumInGame.Sound.shoot: { audio.PlayOneShot(DungeonController.instance.assetLoader.shoot); break; }
            case EnnumInGame.Sound.walk: { audio.PlayOneShot(DungeonController.instance.assetLoader.walk); break; }
            case EnnumInGame.Sound.awaken: { audio.PlayOneShot(DungeonController.instance.assetLoader.awakenV,0.3f); audio.PlayOneShot(DungeonController.instance.assetLoader.awaken); break; }
            case EnnumInGame.Sound.sleep: { audio.PlayOneShot(DungeonController.instance.assetLoader.sleepV,0.3f); audio.PlayOneShot(DungeonController.instance.assetLoader.sleep); break; }
            case EnnumInGame.Sound.pizza: { audio.PlayOneShot(DungeonController.instance.assetLoader.pizzaV,0.3f); audio.PlayOneShot(DungeonController.instance.assetLoader.pizza); break; }
            case EnnumInGame.Sound.lavalamp: { audio.PlayOneShot(DungeonController.instance.assetLoader.lavalampV,0.3f); audio.PlayOneShot(DungeonController.instance.assetLoader.lavalampV); break; }
            case EnnumInGame.Sound.enter: { audio.PlayOneShot(DungeonController.instance.assetLoader.enter); break; }
            case EnnumInGame.Sound.hungry: { audio.PlayOneShot(DungeonController.instance.assetLoader.sleep); break; }
        }
    }
}
