﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    float bulletRange = 3f;
    float lastCall = 0;
    Vector2 tmpPosNow;

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.S))
            ShootBullet(EnnumInGame.Direction.south);

        if (Input.GetKeyDown(KeyCode.W))
            ShootBullet(EnnumInGame.Direction.north);

        if (Input.GetKeyDown(KeyCode.A))
            ShootBullet(EnnumInGame.Direction.west);

        if (Input.GetKeyDown(KeyCode.D))
            ShootBullet(EnnumInGame.Direction.east);
    }

    private void ResetPos()
    {
        transform.position = tmpPosNow;
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
    }

    private void ShootBullet(EnnumInGame.Direction direction)
    {
        if(Time.timeSinceLevelLoad-lastCall>0.1)
        {
            tmpPosNow = transform.position;
            lastCall = Time.timeSinceLevelLoad;
            GameObject bullet = GetComponent<Player>().GetBulletFromPool();
            bullet.transform.position = transform.position;
            bullet.GetComponent<Bullet>().currentPosition = transform.position;
            bullet.GetComponent<Bullet>().destPosition = bullet.GetComponent<Bullet>().currentPosition;

            switch (direction)
            {
                case EnnumInGame.Direction.north:
                    {
                        bullet.GetComponent<Bullet>().destPosition.y += bulletRange;
                        break;
                    }
                case EnnumInGame.Direction.south:
                    {
                        bullet.GetComponent<Bullet>().destPosition.y -= bulletRange;
                        break;
                    }
                case EnnumInGame.Direction.east:
                    {
                        bullet.GetComponent<Bullet>().destPosition.x += bulletRange;
                        break;
                    }
                case EnnumInGame.Direction.west:
                    {
                        bullet.GetComponent<Bullet>().destPosition.x -= bulletRange;
                        break;
                    }
            }
            bullet.GetComponent<Bullet>().RunBullet();
            //GetComponent<Player>().ShootBullet();
            Invoke("ResetPos", 0.1f);
        }
        
        
    }
}
